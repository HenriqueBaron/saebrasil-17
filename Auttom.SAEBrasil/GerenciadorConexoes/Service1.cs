﻿using GerenciadorConexoes.ControleRobo;
using GerenciadorConexoes.Dominio;
using GerenciadorConexoes.Persist.CLP;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace GerenciadorConexoes
{
    public partial class Service1 : ServiceBase
    {
        EventLog eventLog;

        List<Pedido> pedidos;
        ConexaoCLP conexaoCLP;
        GerenciadorRobo controleRobo;
        System.Timers.Timer timer;

        string caminhoArquivoNome;

        public Service1()
        {
            InitializeComponent();

            // Criação do Event Log
            if (!EventLog.SourceExists("EventSourceGatewayI40"))
            {
                EventLog.CreateEventSource("EventSourceGatewayI40", "LogGwI40");
            }
            eventLog = new EventLog()
            {
                Source = "EventSourceGatewayI40",
                Log = "LogGwI40"
            };
        }

        #region Inicialização

        protected override void OnStart(string[] args)
        {
            RegistrarLog("Inicializando...");

            // Abre o XML de configurações e lê os parâmetros necessários.
            int quantidadeUsuarios = Convert.ToInt32(ConfigurationManager.AppSettings["QuantidadeUsuarios"]);
            string enderecoIpServidorModbus = ConfigurationManager.AppSettings["IPServidor"];
            string enderecoIpRobo = ConfigurationManager.AppSettings["IPRobo"];
            int timeoutRespostaRobo = Convert.ToInt32(ConfigurationManager.AppSettings["TimeoutRespostaRobo"]);
            caminhoArquivoNome = ConfigurationManager.AppSettings["CaminhoArquivoImpressora"].@ToString();

            bool ativarSimulacao = Boolean.Parse(ConfigurationManager.AppSettings["SimulacaoRobo"]);
            if (ativarSimulacao) RegistrarLog("O modo de simulação do robô está ativado!");

            // Inicia as instâncias de objetos e inicia o servidor Modbus TCP/IP.
            pedidos = new List<Pedido>();
            conexaoCLP = new ConexaoCLP(enderecoIpServidorModbus, quantidadeUsuarios);
            controleRobo = new GerenciadorRobo(enderecoIpRobo, ativarSimulacao)
            {
                TimeoutRespostaRobo = timeoutRespostaRobo
            };

            try { conexaoCLP.IniciarServidor(); }
            catch (Exception ex)
            {
                RegistrarLog("Erro ao iniciar o servidor Modbus TCP/IP. Exceção lançada: " + ex.Message);
            }

            // Assina os eventos do CLP, de solicitação de início de pedido e confirmação de recebimento.
            conexaoCLP.IniciarPedidoEvent += (sender, eventArgs) =>
            {
                IniciarPedidoEventArgs pedidoArgs = (IniciarPedidoEventArgs)eventArgs;
                InserirPedido(pedidoArgs.NovoPedido);
            };

            conexaoCLP.ConfirmarEntregaEvent += (sender, eventArgs) =>
            {
                ConfirmarEntregaEventArgs pedidoArgs = (ConfirmarEntregaEventArgs)eventArgs;
                FinalizarPedido(pedidoArgs.Usuario);
            };

            // Assina os eventos do robô, de finalização de produção e de entrega.
            controleRobo.ProducaoConcluidaEvent += (sender, eventArgs) => ConcluirProducao();
            controleRobo.PedidoEntregueEvent += (sender, eventArgs) => ConcluirEntrega();

            // Configura um temporizador para disparar as leituras no robô e no servidor periodicamente.
            timer = new System.Timers.Timer
            {
                Interval = 1000,
            };
            timer.Elapsed += new System.Timers.ElapsedEventHandler(OnTimer);
            timer.Start();

            RegistrarLog("Inicialização finalizada.");
        }

        protected override void OnStop()
        {
            RegistrarLog("Finalizando o serviço e encerrando o servidor Modbus TCP/IP.");
            conexaoCLP.PararServidor();
        }

        #endregion

        #region Operações em pedidos

        /// <summary>
        /// Insere um novo pedido na lista, se ainda não existir.
        /// </summary>
        /// <param name="pedido">Pedido a ser inserido.</param>
        private void InserirPedido(Pedido pedido)
        {
            if (!VerificarSePedidoExiste(pedido))
            {
                pedido.Status = Pedido.PedidoStatus.Recebido;
                pedidos.Add(pedido);
                conexaoCLP.AtualizarStatusPedido(pedido);
                RegistrarLog(string.Format("Pedido recebido do usuário {0}, contendo o nome {1} e a cor {2}.",
                    pedido.Usuario, pedido.Nome, pedido.Cor.ToString()));
            }
        }

        /// <summary>
        /// Altera para "Expedido" o status do pedido que estiver em produção no momento.
        /// </summary>
        private void ConcluirProducao()
        {
            IEnumerable<Pedido> pedidosEmProducao = pedidos.Where(p => p.Status == Pedido.PedidoStatus.EmProducao);
            if (pedidosEmProducao.Count() > 0)
            {
                int indicePedido = pedidos.IndexOf(pedidosEmProducao.ElementAt(0));
                pedidos[indicePedido].Status = Pedido.PedidoStatus.Expedido;
                controleRobo.AtualizarStatusPedido(Pedido.PedidoStatus.Expedido);
                conexaoCLP.AtualizarStatusPedido(pedidos[indicePedido]);
                RegistrarLog(string.Format("Produção do pedido do usuário {0} concluída.", pedidos[indicePedido].Usuario));
            }
        }

        /// <summary>
        /// Altera para "Entregue" o status do pedido que estiver em processo de entrega no momento.
        /// </summary>
        private void ConcluirEntrega()
        {
            IEnumerable<Pedido> pedidosExpedidos = pedidos.Where(p => p.Status == Pedido.PedidoStatus.Expedido);
            if (pedidosExpedidos.Count() > 0)
            {
                int indicePedido = pedidos.IndexOf(pedidosExpedidos.ElementAt(0));
                pedidos[indicePedido].Status = Pedido.PedidoStatus.Entregue;
                controleRobo.AtualizarStatusPedido(Pedido.PedidoStatus.Entregue);
                conexaoCLP.AtualizarStatusPedido(pedidos[indicePedido]);
                RegistrarLog(string.Format("Pedido do usuário {0} entregue.", pedidos[indicePedido].Usuario));
            }
        }

        /// <summary>
        /// Finaliza e exlcui o pedido do usuário especificado, se ele possuir status "Entregue".
        /// </summary>
        /// <param name="usuario">Número de usuário cujo pedido deve ser excluído.</param>
        private void FinalizarPedido(int usuario)
        {
            IEnumerable<Pedido> pedidosEntregues = pedidos.Where(p => p.Usuario == usuario);
            if (pedidosEntregues.Count() > 0 && pedidosEntregues.ElementAt(0).Status == Pedido.PedidoStatus.Entregue)
            {
                controleRobo.FinalizarPedido();
                Pedido pedidoRedefinido = pedidosEntregues.ElementAt(0);
                pedidoRedefinido.Status = Pedido.PedidoStatus.NaoRecebido;
                conexaoCLP.AtualizarStatusPedido(pedidoRedefinido);
                pedidos.Remove(pedidosEntregues.ElementAt(0));
                RegistrarLog(string.Format("Pedido do usuário {0} recebido.", pedidoRedefinido.Usuario));
            }
        }

        /// <summary>
        /// Verifica se não há pedido em produção e se há pedidos aguardando, para processar a produção.
        /// </summary>
        private async void AtualizarPedidosAsync()
        {
            IEnumerable<Pedido> pedidosEmProducao = pedidos.Where(p => p.Status >= Pedido.PedidoStatus.EmProducao);
            if (pedidosEmProducao.Count() == 0)
            {
                IEnumerable<Pedido> pedidosAguardando = pedidos.Where(p => p.Status == Pedido.PedidoStatus.Recebido);
                if (pedidosAguardando.Count() > 0)
                {
                    int indicePedido = pedidos.IndexOf(pedidosAguardando.ToList()[0]);

                    Pedido pedidoAProduzir = pedidos[indicePedido];
                    pedidoAProduzir.Status = Pedido.PedidoStatus.EmProducao;

                    Task imprimirNomeTask = EnviarParaImpressora(pedidoAProduzir.Nome);

                    Task iniciarPedidoTask = controleRobo.IniciarPedidoAsync(pedidoAProduzir);

                    try
                    {
                        await imprimirNomeTask;
                        await iniciarPedidoTask;
                        pedidos.Find((p) => p.Usuario == pedidoAProduzir.Usuario).Status = Pedido.PedidoStatus.EmProducao;
                        conexaoCLP.AtualizarStatusPedido(pedidoAProduzir);
                        RegistrarLog(string.Format("Pedido do usuário {0} iniciado.", pedidoAProduzir.Usuario));
                    }
                    catch (Exception ex)
                    {
                        RegistrarLog("Erro ao iniciar o pedido. Exceção lançada: " + ex.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Verifica se o pedido de um usuário especificado já existe.
        /// </summary>
        /// <param name="pedido">Pedido cujo número de usuário deseja-se verificar.</param>
        /// <returns>Retorna verdadeiro se o pedido já existe.</returns>
        private bool VerificarSePedidoExiste(Pedido pedido)
        {
            IEnumerable<Pedido> pedidosRepetidos = pedidos.Where(p => p.Usuario == pedido.Usuario);
            return pedidosRepetidos.Count() != 0;
        }

        #endregion

        private async Task EnviarParaImpressora(string texto)
        {
            ConexaoImpressora impressora = new ConexaoImpressora(caminhoArquivoNome);
            try
            {
                await impressora.AtualizarNomeAsync(texto);
                RegistrarLog(string.Format("O nome {0} foi enviado para a impressora.", texto));
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao enviar nome para impressora. Exceção lançada: " + ex.Message);
            }
        }

        /// <summary>
        /// A cada contagem de tempo do timer, atualiza as leituras no CLP e no robô.
        /// </summary>
        private void OnTimer(object sender, EventArgs eventArgs)
        {
            AtualizarPedidosAsync();
            conexaoCLP.AtualizarBitsComando();
            if (pedidos.Count > 0)
            {
                try { controleRobo.AtualizarLeituras(); }
                catch (Exception ex)
                {
                    RegistrarLog("Erro ao ler dados do robô. Exceção lançada: " + ex.Message);
                }
            }
        }

        /* Este método é destinado apenas para testes em que este serviço é inicializado
         * como uma aplicação de console. */
        internal void TestStartupAndStop()
        {
            OnStart(null);
            Console.ReadLine();
            OnStop();
        }

        /// <summary>
        /// Registra uma mensagem, dependendo do ambiente em que a aplicação está rodando.
        /// </summary>
        /// <param name="mensagem">Mensagem a ser registrada.</param>
        private void RegistrarLog(string mensagem)
        {
            // Caso uma aplicação de console esteja sendo executada, mensagens ali também são
            // escritas.
            if (Environment.UserInteractive)
            {
                Console.WriteLine(mensagem);
            }
            eventLog.WriteEntry(mensagem); // Escreve de qualquer forma no Event Log do Visualizador de Eventos do Windows.
        }
    }
}
