﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GerenciadorConexoes
{
    /// <summary>
    /// Estabelece as funções necessárias para conectar com o computador da impressora e atualizar o nome a ser
    /// impresso.
    /// </summary>
    public class ConexaoImpressora
    {
        private string caminhoArquivoCompartilhado;

        /// <summary>
        /// Inicializa uma nova instâcia da classe ConexaoImpressora.
        /// </summary>
        /// <param name="caminhoCompartilhamento">Arquivo compartilhado onde o texto de impressão deve ser escrito.</param>
        public ConexaoImpressora(string caminhoCompartilhamento)
        {
            caminhoArquivoCompartilhado = caminhoCompartilhamento;
        }

        /// <summary>
        /// Solicita de maneira assíncrona a atualização do nome para que a impressora imprima.
        /// </summary>
        /// <param name="nome">Nome a ser impresso.</param>
        public async Task AtualizarNomeAsync(string nome)
        {
            using (StreamWriter sw = new StreamWriter(@caminhoArquivoCompartilhado, false, Encoding.Unicode))
            {
                await sw.WriteAsync(nome);
            }
        }
    }
}
