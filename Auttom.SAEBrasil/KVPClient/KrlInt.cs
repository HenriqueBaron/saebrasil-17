﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KUKAVarProxyInterface
{
    public class KrlInt : KrlVariable
    {
        public int Valor { get => (int)valor; set => valor = value; }

        protected override void StringParaValor(string valor)
        {
            Valor = Convert.ToInt32(valor);
        }

        protected override string ValorParaString()
        {
            return Valor.ToString();
        }
    }
}
