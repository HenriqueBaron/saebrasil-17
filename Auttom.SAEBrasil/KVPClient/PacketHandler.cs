﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KUKAVarProxyInterface
{
    internal static class PacketHandler
    {
        static short requestID = 0;

        public static short GerarRequestID()
        {
            requestID++;
            return requestID;
        }

        private static byte[] RequestIDToBytes(short requestID)
        {
            //Formata o request ID fornecido para um array de 2 bytes
            byte[] resultado = new byte[2];
            resultado[0] = (byte)(requestID >> 8 & 255);
            resultado[1] = (byte)(requestID & 255);
            return resultado;
        }

        private static byte[] TamanhoNomeToBytes(string nome)
        {
            //Converte o tamanho do nome da variável para um vetor de dois bytes
            byte[] resultado = new byte[2];
            resultado[0] = (byte)(nome.Length >> 8 & 255);
            resultado[1] = (byte)(nome.Length & 255);
            return resultado;
        }

        private static byte[] NomeToBytes(string nome)
        {
            //Obtém os bytes para o nome da variável
            char[] nomeChars = nome.ToCharArray();
            byte[] resultado = new byte[nomeChars.Length];
            for (int i = 0; i < resultado.Length; i++)
            {
                resultado[i] = Convert.ToByte(nomeChars[i]);
            }
            return resultado;
        }

        private static byte[] TamanhoValorToBytes(string valor)
        {
            //Converte o tamanho do valor da variável (em caracteres) para um vetor de dois bytes
            byte[] resultado = new byte[2];
            resultado[0] = (byte)(valor.Length >> 8 & 255);
            resultado[1] = (byte)(valor.Length & 255);
            return resultado;
        }

        private static byte[] ValorToBytes(string valor)
        {
            //Obtém os bytes para o valor da variável (em caracteres)
            char[] valorChars = valor.ToCharArray();
            byte[] resultado = new byte[valorChars.Length];
            for (int i = 0; i < resultado.Length; i++)
            {
                resultado[i] = Convert.ToByte(valorChars[i]);
            }
            return resultado;
        }

        private static short BytesToShort(byte[] pacote, int index)
        {
            byte[] subPacote = { pacote[index], pacote[index + 1] };
            return (short)((subPacote[0] << 8) + subPacote[1]);
        }

        private static string BytesToString(byte[] pacote, int index, int length)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < length; i++)
            {
                sb.Append(Convert.ToChar(pacote[index + i]));
            }
            return sb.ToString();
        }

        public static byte[] MontarRequest(string nomeLeitura, short requestID)
        {
            /* Estrutura do pacote de leitura (exemplo):
            *  0   1     2   3       4         5   6
            *  xx  xx  | 00  0A  |   00   |   00  07     |  24  4F  56  5F  50  52  4F
            *          |   10    |   0    |       7      |  $   O   V   _   P   R   O
            *  REQ ID  | REQ LEN | READ=0 | VAR NAME LEN |  VAR NAME CHARS
            * (random)*/
            //Monta um pacote de pedido de leitura

            //Obtém os componentes necessários
            byte[] requestIDBytes = RequestIDToBytes(requestID);
            byte[] tamanhoNome = TamanhoNomeToBytes(nomeLeitura);
            byte[] nome = NomeToBytes(nomeLeitura);

            //Calcula o tamanho total do pacote. Um byte ao final é reservado para ficar em branco.
            int tamanhoPacote = requestIDBytes.Length + 4 + tamanhoNome.Length + nome.Length;
            byte[] pacote = new byte[tamanhoPacote];

            //Calcula o tamanho do request, que é um dos componentes do pacote
            //O tamanho do request para a leitura é contado desde o byte 4 (READ/WRITE)
            //até o final do pacote.
            int tamanhoRequest = tamanhoNome.Length + nome.Length + 1;
            byte[] tamanhoRequestBytes = new byte[2];
            tamanhoRequestBytes[0] = (byte)(tamanhoRequest >> 8 & 255);
            tamanhoRequestBytes[1] = (byte)(tamanhoRequest & 255);

            //Monta o pacote com os componentes obtidos
            pacote[0] = requestIDBytes[0];
            pacote[1] = requestIDBytes[1];
            pacote[2] = tamanhoRequestBytes[0];
            pacote[3] = tamanhoRequestBytes[1];
            pacote[4] = 0;
            pacote[5] = tamanhoNome[0];
            pacote[6] = tamanhoNome[1];
            nome.CopyTo(pacote, 7);

            return pacote;
        }

        public static byte[] MontarRequest(string nomeEscrita, string valorEscrita, short requestID)
        {
            /* Estrutura do pacote de escrita (exemplo):
           *  0  1     2   3       4         5   6
           *  xx xx  | 00  10  |   01    |   00  07     | 24 4F 56 5F 50 52 4F | 00 02   |  38  36
           *         |   16    |   1     |       7      | $  O  V  _  P  R  O  |     2   |  8   6
           * REQ ID  | REQ LEN | WRITE=1 | VAR NAME LEN | VAR NAME CHARS       | VAL LEN | VAL AS STRING
           * (random)*/
            //Monta um pacote de pedido de escrita

            //Obtém os componentes necessários
            byte[] requestIDBytes = RequestIDToBytes(requestID);
            byte[] tamanhoNome = TamanhoNomeToBytes(nomeEscrita);
            byte[] nome = NomeToBytes(nomeEscrita);
            byte[] tamanhoValor = TamanhoValorToBytes(valorEscrita);
            byte[] valor = ValorToBytes(valorEscrita);

            //Calcula o tamanho do pacote
            int tamanhoPacote = requestIDBytes.Length +
                                tamanhoNome.Length +
                                nome.Length +
                                tamanhoValor.Length +
                                valor.Length + 4;
            byte[] pacote = new byte[tamanhoPacote];

            //Calcula o tamanho do request, que é um dos componentes do pacote
            //O tamanho do request para a escrita é contado desde o byte 2 (REQ LEN)
            //até o final do pacote.
            int tamanhoRequest = tamanhoNome.Length +
                                 nome.Length +
                                 tamanhoValor.Length +
                                 valor.Length + 3;
            byte[] tamanhoRequestBytes = new byte[2];
            tamanhoRequestBytes[0] = (byte)(tamanhoRequest >> 8 & 255);
            tamanhoRequestBytes[1] = (byte)(tamanhoRequest & 255);

            //Monta o pacote com os componentes obtidos
            pacote[0] = requestIDBytes[0];
            pacote[1] = requestIDBytes[1];
            pacote[2] = tamanhoRequestBytes[0];
            pacote[3] = tamanhoRequestBytes[1];
            pacote[4] = 1;
            pacote[5] = tamanhoNome[0];
            pacote[6] = tamanhoNome[1];
            nome.CopyTo(pacote, 7);
            tamanhoValor.CopyTo(pacote, 7 + nome.Length);
            valor.CopyTo(pacote, 7 + nome.Length + tamanhoValor.Length);

            return pacote;
        }

        public static string AbrirResponseLeitura(byte[] pacote, short requestID)
        {
            /* Estrutura do pacote de leitura (exemplo):
            *  0   1     2   3       4         5   6
            *  xx  xx  | 00  08  |   00   |   00  02  | 33  35      | 00  | 01 01
            *          |   8     |   0    |       2   | 3   5       |  0  | 1  1
            *  SAME AS | RSP LEN | READ=0 | VALUE LEN | VALUE CHARS | PAD | READ OK IF 01 01
            *  REQUEST |*/
            //Abre um pacote de resposta de leitura
            short responseID = BytesToShort(pacote, 0);
            if (responseID != requestID)
            {
                //Se os IDs de mensagem não forem iguais
                throw new ArgumentException("O response ID obtido não é igual ao request ID fornecido.");
            }
            short tamanhoResponse = BytesToShort(pacote, 2);
            if (pacote[tamanhoResponse + 3] != 1 || pacote[tamanhoResponse + 2] != 1)
            {
                //Se os últimos dois bytes da mensagem não forem iguais a 1
                throw new ArgumentException("A resposta recebida é inválida.");
            }
            short tamanhoValor = BytesToShort(pacote, 5);
            if (tamanhoValor == 0)
            {
                throw new ArgumentException("O tamanho do valor recebido é nulo.");
            }
            return BytesToString(pacote, 7, tamanhoValor);
        }

        public static void VerificarResponseEscrita(byte[] pacote, short requestID)
        {
            /* Estrutura do pacote de escrita (exemplo):
            *  0   1     2   3       4         5   6
            *  xx  xx  | 00  08  |   01   |   00  02  | 38  36      | 00  | 01 01
            *          |   8     |   1    |       2   | 8   6       |  0  |  1  1
            *  SAME AS | RSP LEN | WRITE=1| VALUE LEN | WRITTEN VAL | PAD | READ OK IF 01 01
            *  REQUEST |*/
            //Abre um pacote de resposta de escrita
            short responseID = BytesToShort(pacote, 0);
            if (responseID != requestID)
            {
                //Se os IDs de mensagem não forem iguais
                throw new ArgumentException("O response ID obtido não é igual ao request ID fornecido.");
            }
            short tamanhoResponse = BytesToShort(pacote, 2);
            if (pacote[tamanhoResponse + 3] != 1 || pacote[tamanhoResponse + 2] != 1)
            {
                //Se os últimos dois bytes da mensagem não forem iguais a 1
                throw new ArgumentException("A resposta recebida é inválida.");
            }
        }
    }
}
