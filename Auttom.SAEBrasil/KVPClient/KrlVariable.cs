﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KUKAVarProxyInterface
{
    public abstract class KrlVariable
    {
        protected object valor;

        public string Nome { get; set; }
        public string ValorString
        {
            get => ValorParaString();
            set => StringParaValor(value);
        }

        protected abstract string ValorParaString();
        protected abstract void StringParaValor(string valor);

        public void Escrever(KvpClient kvpClient)
        {
            kvpClient.EscreverVariavel(this);
        }

        public T Ler<T>(KvpClient kvpClient)
        {
            return (T)kvpClient.LerVariavel(this).valor;
        }
    }
}
