﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;

namespace KUKAVarProxyInterface
{
    public class KvpClient
    {
        private TcpClient tcpClient;
        private bool conectado = false;

        public bool Conectado { get => conectado; }

        public void Conectar(string enderecoIP, int timeout)
        {
            //Cria o socket e realiza a conexão TCP no endereço especificado, na porta TCP 7000
            tcpClient = new TcpClient
            {
                SendTimeout = timeout,
                ReceiveTimeout = timeout
            };
            IAsyncResult resultado = tcpClient.BeginConnect(enderecoIP, 7000, null, null);
            resultado.AsyncWaitHandle.WaitOne(timeout, true);

            if (!tcpClient.Connected) throw new Exception("A conexão com o robô não pôde ser estabelecida. " +
                "Verifique o estado da conexão e se a porta TCP 7000 está desbloqueada no KR C4.");
        }

        public void Desconectar()
        {
            conectado = false;
            tcpClient.Close();
        }

        public KrlVariable LerVariavel(KrlVariable variavel)
        {
            //Verifica se o KUKAVARPROXY está conectado e prepara o pacote para envio
            if (!tcpClient.Connected) return null;
            short requestID = PacketHandler.GerarRequestID();
            byte[] pacoteAEnviar = PacketHandler.MontarRequest(variavel.Nome, requestID);

            byte[] pacoteRecebido = TrocarMensagem(pacoteAEnviar);

            KrlVariable resultado = variavel;
            resultado.ValorString = PacketHandler.AbrirResponseLeitura(pacoteRecebido, requestID);
            return resultado;
        }

        public List<KrlVariable> LerVariaveis(List<KrlVariable> variaveis)
        {
            List<KrlVariable> variaveisLidas = new List<KrlVariable>();
            foreach (KrlVariable item in variaveis)
            {
                variaveisLidas.Add(LerVariavel(item));
            }
            return variaveisLidas;
        }

        public void EscreverVariavel(KrlVariable variavel)
        {
            //Verifica se o KUKAVARPROXY está conectado e prepara o pacote para envio
            if (!tcpClient.Connected) return;
            short requestID = PacketHandler.GerarRequestID();
            byte[] pacoteAEnviar = PacketHandler.MontarRequest(variavel.Nome, variavel.ValorString, requestID);

            byte[] pacoteRecebido = TrocarMensagem(pacoteAEnviar);

            PacketHandler.VerificarResponseEscrita(pacoteRecebido, requestID);
        }

        public void EscreverVariaveis(List<KrlVariable> variaveis)
        {
            foreach (KrlVariable item in variaveis)
            {
                EscreverVariavel(item);
            }
        }

        private byte[] TrocarMensagem(byte[] pacoteAEnviar)
        {
            //Instancia o stream de dados, então envia o pacote e aguarda uma resposta
            NetworkStream streamServidor = tcpClient.GetStream();
            streamServidor.Write(pacoteAEnviar, 0, pacoteAEnviar.Length);
            streamServidor.Flush();

            byte[] pacoteRecebido = new byte[tcpClient.ReceiveBufferSize];
            streamServidor.Read(pacoteRecebido, 0, pacoteRecebido.Length);
            return pacoteRecebido;
        }
    }
}
