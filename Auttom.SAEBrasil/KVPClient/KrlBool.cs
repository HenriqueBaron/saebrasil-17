﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KUKAVarProxyInterface
{
    public class KrlBool : KrlVariable
    {
        public bool Valor { get => (bool)valor; set => valor = value; }

        protected override void StringParaValor(string valor)
        {
            Valor = valor == "TRUE" ? true : false;
        }

        protected override string ValorParaString()
        {
            return Valor ? "TRUE" : "FALSE";
        }
    }
}
