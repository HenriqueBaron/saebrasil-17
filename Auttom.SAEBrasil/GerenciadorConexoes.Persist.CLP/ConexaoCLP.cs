﻿using GerenciadorConexoes.Dominio;
using Modbus;
using NetFwTypeLib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace GerenciadorConexoes.Persist.CLP
{
    /// <summary>
    /// Define uma conexão com o CLP para atuar como servidor. Eventos são gerados para notificar
    /// a classe que instancia este objeto de que valores em relação aos pedidos foram alterados.
    /// </summary>
    public class ConexaoCLP
    {
        private string enderecoIpServidor;
        private int quantidadeUsuarios = 10;
        private ModbusSlaveTCP modbusServer;
        private bool[] estadosAnterioresInicio;
        private bool[] estadosAnterioresConfirmar;

        public event EventHandler IniciarPedidoEvent;
        public event EventHandler ConfirmarEntregaEvent;

        public ConexaoCLP(string enderecoIpServidor, int quantidadeUsuarios = 10)
        {
            this.enderecoIpServidor = enderecoIpServidor;
            this.quantidadeUsuarios = quantidadeUsuarios;
            estadosAnterioresInicio = new bool[quantidadeUsuarios];
            estadosAnterioresConfirmar = new bool[quantidadeUsuarios];
        }

        /// <summary>
        /// Inicia o servidor Modbus TCP/IP com o Unit ID valendo 1, e utilizando a porta 502.
        /// </summary>
        public void IniciarServidor()
        {
            try
            {
                LiberarFirewall();
                Datastore datastore = new Datastore(1);
                modbusServer = new ModbusSlaveTCP(new Datastore[] { datastore }, System.Net.IPAddress.Parse(enderecoIpServidor), 502);
                modbusServer.StartListen();
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível iniciar o servidor " +
                    "Modbus TCP. Exceção lançada: " + ex.Message);
            }
        }

        /// <summary>
        /// Desabilita o servidor Modbus TCP e para a atualização das leituras.
        /// </summary>
        public void PararServidor()
        {
            modbusServer.StopListen();
        }

        /// <summary>
        /// Verifica se uma regra para permissão de entrada de dados na porta TCP 502 (necessário para o Modbus) já existe e,
        /// se não houver, cria-a.
        /// </summary>
        private void LiberarFirewall()
        {
            bool regraJaExiste = false;
            INetFwPolicy2 fwPolicy = (INetFwPolicy2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FwPolicy2"));
            foreach (INetFwRule rule in fwPolicy.Rules)
            {
                if (rule.Name == "Modbus TCP/IP In")
                {
                    regraJaExiste = true;
                    break;
                }
            }
            if (!regraJaExiste)
            {
                INetFwRule fwRule = (INetFwRule)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FWRule"));
                fwRule.Action = NET_FW_ACTION_.NET_FW_ACTION_ALLOW;
                fwRule.Name = "Modbus TCP/IP In";
                fwRule.Description = "Entrada de dados via Modbus TCP/IP";
                fwRule.Direction = NET_FW_RULE_DIRECTION_.NET_FW_RULE_DIR_IN;
                fwRule.Protocol = 6; // 6 para TCP, segundo site http://iana.org
                fwRule.LocalPorts = "502";
                fwRule.Enabled = true;
                fwRule.InterfaceTypes = "All";

                fwPolicy.Rules.Add(fwRule);
            }
        }

        /// <summary>
        /// Monitora se houve alterações nos bits de comando dos pedidos no CLP. Este método é executado
        /// de maneira cíclica e permanente, em uma thread separada.
        /// </summary>
        public void AtualizarBitsComando()
        {
            bool[] estadosAtuaisInicio = new bool[quantidadeUsuarios];
            bool[] estadosAtuaisConfirmar = new bool[quantidadeUsuarios];
            // Verifica o status dos bits de iniciar pedido e confirmar entrega de cada um dos usuários
            for (int i = 0; i < quantidadeUsuarios; i++)
            {
                ushort controlWord = modbusServer.ModbusDB[0].HoldingRegisters[111 + i];
                BitArray bitArray = new BitArray(new int[] { controlWord });
                estadosAtuaisInicio[i] = bitArray[0];
                estadosAtuaisConfirmar[i] = bitArray[1];
            }
            VerificarAlteracoesInicio(estadosAtuaisInicio);
            VerificarAlteracoesConfirmar(estadosAtuaisConfirmar);
        }

        private void VerificarAlteracoesInicio(bool[] estadosAtuaisInicio)
        {
            for (int i = 0; i < estadosAtuaisInicio.Length; i++)
            {
                // Caso o status do comando de início tenha mudado e seja verdadeiro
                if ((estadosAtuaisInicio[i] ^ estadosAnterioresInicio[i]) && estadosAtuaisInicio[i])
                {
                    // Um novo pedido é criado e o evento correspondente é disparado.
                    Pedido novoPedido = new Pedido
                    {
                        Usuario = i,
                        Nome = GetNome(i),
                        Cor = (Pedido.PedidoCor)modbusServer.ModbusDB[0].HoldingRegisters[101 + i],
                        Status = Pedido.PedidoStatus.Recebido
                    };
                    IniciarPedidoEventArgs args = new IniciarPedidoEventArgs
                    {
                        NovoPedido = novoPedido
                    };
                    OnIniciarPedido(args);
                }

            }
            estadosAnterioresInicio = estadosAtuaisInicio;
        }

        private void VerificarAlteracoesConfirmar(bool[] estadosAtuaisConfirmar)
        {
            for (int i = 0; i < estadosAtuaisConfirmar.Length; i++)
            {
                // Caso o status do comando de confirmar recebimento tenha mudado e seja verdadeiro
                if ((estadosAtuaisConfirmar[i] ^ estadosAnterioresConfirmar[i]) && estadosAtuaisConfirmar[i])
                {
                    // O evento que indica a confirmação é disparado, fornecendo o usuário que confirmou.
                    ConfirmarEntregaEventArgs args = new ConfirmarEntregaEventArgs()
                    {
                        Usuario = i
                    };
                    OnConfirmarEntrega(args);
                }
            }
            estadosAnterioresConfirmar = estadosAtuaisConfirmar;
        }

        /// <summary>
        /// Obtém da base de dados Modbus TCP o nome do usuário nos registros a partir do código de usuário fornecido.
        /// </summary>
        /// <param name="usuario">Código do usuário para qual se deseja o nome.</param>
        /// <returns>Retorna o nome fornecido pelo usuário.</returns>
        private string GetNome(int usuario)
        {
            /* Para cada um dos 10 registros que contém os 20 caracteres do nome, é feita a varredura que
             * converte uma word (ushort) para um par de caracteres, sendo todos concatenados ao final */
            List<ushort> wordsNome = modbusServer.ModbusDB[0].HoldingRegisters.ToList().GetRange(10 * usuario + 1, 10);
            List<char> caracteres = new List<char>();
            foreach (ushort item in wordsNome)
            {
                ushort parteEsquerda = (ushort)(item >> 8 & 255);
                ushort parteDireita = (ushort)(item & 255);

                if (parteEsquerda != 0) caracteres.Add(Convert.ToChar(parteEsquerda));
                if (parteDireita != 0) caracteres.Add(Convert.ToChar(parteDireita));
            }
            return new string(caracteres.ToArray());
        }

        private string FormatarNome(string nomeOriginal)
        {
            if (nomeOriginal.Contains(" "))
            {
                string resultado = nomeOriginal.Replace(" ", "\r\n");
                return resultado;
            }
            else return nomeOriginal;
        }

        protected virtual void OnIniciarPedido(IniciarPedidoEventArgs e)
        {
            IniciarPedidoEvent?.Invoke(this, e);
        }

        protected virtual void OnConfirmarEntrega(ConfirmarEntregaEventArgs e)
        {
            ConfirmarEntregaEvent?.Invoke(this, e);
        }

        /// <summary>
        /// Atualiza o status do pedido especificado baseado na sua propriedade "Usuário".
        /// </summary>
        /// <param name="pedido">Pedido contendo o novo status.</param>
        public void AtualizarStatusPedido(Pedido pedido)
        {
            modbusServer.ModbusDB[0].HoldingRegisters[161 + pedido.Usuario] = (ushort)pedido.Status;
        }
    }
}