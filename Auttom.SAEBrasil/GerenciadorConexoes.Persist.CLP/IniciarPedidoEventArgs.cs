﻿using GerenciadorConexoes.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GerenciadorConexoes.Persist.CLP
{
    public class IniciarPedidoEventArgs : EventArgs
    {
        public Pedido NovoPedido { get; set; }
    }
}
