﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GerenciadorConexoes.Persist.CLP
{
    public class ConfirmarEntregaEventArgs : EventArgs
    {
        public int Usuario { get; set; }
    }
}
