﻿using GerenciadorConexoes.Dominio;
using GerenciadorConexoes.Persist.Robo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GerenciadorConexoes.ControleRobo
{
    public class GerenciadorRobo
    {
        private bool roboPronto = false;
        private bool aivNaEntrega = false;
        private IConexaoRobo conexaoRobo;

        public Pedido PedidoAtual { get; set; }
        public int TimeoutRespostaRobo { get; set; }

        public event EventHandler ProducaoConcluidaEvent;
        public event EventHandler PedidoEntregueEvent;

        public GerenciadorRobo(string ipAddress, bool ativarSimulacao = false)
        {
            if (ativarSimulacao) conexaoRobo = new ConexaoRoboSimulado();
            else conexaoRobo = new ConexaoRoboKuka(ipAddress);
        }

        public void AtualizarLeituras()
        {
            bool roboProntoNovo = conexaoRobo.LerRoboPronto();
            bool aivNaEntregaNovo = conexaoRobo.LerAivNaEntrega();
            VerificarAlteracaoRoboPronto(roboProntoNovo);
            VerificarAlteracaoAivNaEntrega(aivNaEntregaNovo);
            roboPronto = roboProntoNovo;
            aivNaEntrega = aivNaEntregaNovo;
        }

        private void VerificarAlteracaoRoboPronto(bool roboProntoNovo)
        {
            if (roboProntoNovo ^ roboPronto
                && roboProntoNovo
                && PedidoAtual.Status == Pedido.PedidoStatus.EmProducao)
            {
                OnProducaoConcluida(new EventArgs());
            }
        }

        private void VerificarAlteracaoAivNaEntrega(bool aivNaEntregaNovo)
        {
            if (aivNaEntregaNovo ^ aivNaEntrega
                && aivNaEntregaNovo
                && PedidoAtual.Status == Pedido.PedidoStatus.Expedido)
            {
                OnPedidoEntregue(new EventArgs());
            }
        }

        /// <summary>
        /// Caso o robô esteja livre e não haja pedido em processamento, um novo pedido é iniciado.
        /// </summary>
        /// <param name="pedido">Conteúdo do pedido a ser produzido.</param>
        /// <returns>Retorna verdadeiro se o pedido foi iniciado com sucesso.</returns>
        public async Task IniciarPedidoAsync(Pedido pedido)
        {
            await Task.Run(() =>
            {
                roboPronto = conexaoRobo.LerRoboPronto();
                if (PedidoAtual == null && roboPronto)
                {
                    PedidoAtual = pedido;
                    PedidoAtual.Status = Pedido.PedidoStatus.EmProducao;
                    conexaoRobo.EscreverCor(pedido.Cor);
                    conexaoRobo.EscreverIniciarPeca(true);
                    AguardarConfirmacaoRobo();
                }
                else
                {
                    throw new Exception("Impossível iniciar um novo pedido. Outro pedido já " +
                    "está atribuído ao robô, ou ele está ocupado.");
                }
            });
        }

        /// <summary>
        /// Atualiza o status do pedido atual no robô, somente se existe algum pedido em processamento
        /// pelo robô.
        /// </summary>
        /// <param name="status">Novo status que deve ser escrito.</param>
        public void AtualizarStatusPedido(Pedido.PedidoStatus status)
        {
            if (PedidoAtual.Status >= Pedido.PedidoStatus.EmProducao) PedidoAtual.Status = status;
        }

        /// <summary>
        /// Finaliza o pedido atual no robô, caso o seu status seja de pedido entregue.
        /// </summary>
        /// <returns>Retorna verdadeiro caso o pedido tenha sido finalizado - e destruído - com sucesso.</returns>
        public void FinalizarPedido()
        {
            if (PedidoAtual.Status == Pedido.PedidoStatus.Entregue)
            {
                PedidoAtual = null;
            }
            else throw new Exception("Impossível finalizar pedido. O pedido atual não foi entregue ainda.");
        }

        private void AguardarConfirmacaoRobo()
        {
            DateTime emissaoPedido = DateTime.Now;
            while (conexaoRobo.LerRoboPronto())
            {
                Thread.Sleep(1000);
                if (DateTime.Now > emissaoPedido.AddSeconds(TimeoutRespostaRobo))
                    throw new Exception("O robô não confirmou o recebimento do produto.");
            }
            conexaoRobo.EscreverIniciarPeca(false);
        }

        protected virtual void OnProducaoConcluida(EventArgs e)
        {
            ProducaoConcluidaEvent?.Invoke(this, e);
        }

        protected virtual void OnPedidoEntregue(EventArgs e)
        {
            PedidoEntregueEvent?.Invoke(this, e);
        }
    }
}
