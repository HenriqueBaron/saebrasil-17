﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GerenciadorConexoes.Dominio;

namespace GerenciadorConexoes.Persist.Robo
{
    public class ConexaoRoboSimulado : IConexaoRobo
    {
        private static DateTime momentoInicio = DateTime.Now.AddSeconds(-4);
        private static DateTime momentoConclusao = DateTime.Now;
        private static bool emProducao;

        public void EscreverCor(Pedido.PedidoCor cor) { }

        public void EscreverIniciarPeca(bool valor)
        {
            if (valor)
            {
                momentoInicio = DateTime.Now;
                emProducao = true;
            }
        }

        public bool LerAivNaEntrega()
        {
            return DateTime.Now >= momentoConclusao.AddSeconds(10) && !emProducao;
        }

        public bool LerRoboPronto()
        {
            if (DateTime.Now >= momentoInicio.AddSeconds(8) && emProducao)
            {
                momentoConclusao = DateTime.Now;
                emProducao = false;
            }
            return !emProducao;
        }
    }
}
