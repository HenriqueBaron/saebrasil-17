﻿using GerenciadorConexoes.Dominio;
using KUKAVarProxyInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GerenciadorConexoes.Persist.Robo
{
    public class ConexaoRoboKuka : IConexaoRobo
    {
        //private KvpClient kvpClient;
        private string enderecoIp;

        private KrlBool variavelRoboPronto = new KrlBool()
        {
            Nome = "KVP_OUT_xRoboDisponivel"
        };

        private KrlBool variavelAivNaEntrega = new KrlBool()
        {
            Nome = "KVP_OUT_xAivPosEntrega"
        };

        private KrlBool variavelComandoInicio = new KrlBool()
        {
            Nome = "KVP_IN_xStart"
        };

        private KrlInt variavelCor = new KrlInt()
        {
            Nome = "KVP_IN_iCor",
            Valor = 0
        };

        public ConexaoRoboKuka(string enderecoIPRobo)
        {
            enderecoIp = enderecoIPRobo;
        }

        private KvpClient Conectar()
        {
            KvpClient kvpClient = new KvpClient();
            kvpClient.Conectar(enderecoIp, 3000);
            return kvpClient;
        }

        private void Desconectar(KvpClient kvpClient)
        {
            kvpClient.Desconectar();
        }

        public bool LerRoboPronto()
        {
            KvpClient kvpClient = Conectar();
            bool resultado = variavelRoboPronto.Ler<bool>(kvpClient);
            Desconectar(kvpClient);
            return resultado;
        }

        public bool LerAivNaEntrega()
        {
            KvpClient kvpClient = Conectar();
            bool resultado = variavelAivNaEntrega.Ler<bool>(kvpClient);
            Desconectar(kvpClient);
            return resultado;
        }

        public void EscreverIniciarPeca(bool valor)
        {
            variavelComandoInicio.Valor = valor;
            KvpClient kvpClient = Conectar();
            variavelComandoInicio.Escrever(kvpClient);
            Desconectar(kvpClient);
        }

        public void EscreverCor(Pedido.PedidoCor valor)
        {
            variavelCor.Valor = (int)valor;
            KvpClient kvpClient = Conectar();
            variavelCor.Escrever(kvpClient);
            Desconectar(kvpClient);
        }

    }
}
