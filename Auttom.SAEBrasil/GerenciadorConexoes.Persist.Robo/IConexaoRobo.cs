﻿using GerenciadorConexoes.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GerenciadorConexoes.Persist.Robo
{
    public interface IConexaoRobo
    {
        bool LerRoboPronto();
        bool LerAivNaEntrega();
        void EscreverIniciarPeca(bool valor);
        void EscreverCor(Pedido.PedidoCor cor);
    }
}
