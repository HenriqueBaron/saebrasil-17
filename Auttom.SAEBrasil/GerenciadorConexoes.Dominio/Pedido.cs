﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace GerenciadorConexoes.Dominio
{
    public class Pedido
    {
        public enum PedidoStatus { NaoRecebido, Recebido, EmProducao, Expedido, Entregue };
        public enum PedidoCor { Preto, Azul, Vermelho};

        public int Usuario { get; set; }
        public string Nome { get; set; }
        public PedidoCor Cor { get; set; }
        public PedidoStatus Status { get; set; }
    }
}
